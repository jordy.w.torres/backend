'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var auto = models.auto;
var persona = models.persona;
var marca = models.marca;
class AutoController {
    async listar(req, res) {
        console.log("hola");
        var listar = await auto.findAll({
            attributes: [
                'external_id',
                'modelo',
                'costo',
                'placa',
                'color',
                'kilometraje',
                'numeroPuertas',
                'dni_duenio',
                'tipoCombustible',
            ]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, info: listar });
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await auto.findOne({
            where: { external_id: external },
            include: [
                { model: models.persona, attributes: ['apellidos', 'nombres'] },
                { model: models.marca, attributes: ['nombre'] }
            ],
            attributes: [
                'costo',
                'placa',
                'color',
                'kilometraje',
                'numeroPuertas',
                'tipoCombustible',
                'external_id'
            ]
        });
        if (lista == null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: "OK", code: 200, info: lista });
    }
    async cantAutos(req, res) {
        var cant = await auto.count();
        console.log(cant);
        res.json({ msg: "OK!", code: 200, info: cant });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let marca_id = req.body.external_id;
            console.log(marca_id);
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                console.log(marcaAux,"dadasd");
                if (marcaAux) {
                    var data = {
                        id_marca: marcaAux.id,
                        modelo: req.body.modelo,
                        costo: req.body.costo,
                        placa: req.body.placa,
                        color: req.body.color,
                        kilometraje: req.body.kilometraje,
                        numeroPuertas: req.body.numeroPuertas,
                        tipoCombustible: req.body.tipoCombustible,
                        dni_duenio: req.body.dni_duenio
                    };
                    res.status(200);
                    //let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data);
                        //await transaction.commit();
                        res.json({ msg: "Se ha registrado sus datos", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }

            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    async modificar(req, res) {
        var carro = await auto.findOne({ where: { external_id: req.body.external_id}}); 
        if (carro === null) {
            res.json({ msg: "No existe el registro", code: 400 });
        } else {
            carro.costo= req.body.costo;
            carro.color= req.body.color;
            carro.kilometraje= req.body.kilometraje;
            carro.precio = req.body.precio;
            var result = await carro.save();
            if (result === null) {

            } else {
                res.status(200);
                res.json({ msg: "se han modificado sus datos", code: 200 });
            }
        }
    }

    
}

module.exports = AutoController;
