'use strict';
var models = require('../models');
var persona = models.persona;
var auto = models.auto;
var cuenta = models.cuenta;
var marca = models.marca;
var detalle = models.detalle;
var factura = models.factura;
const { body, validationResult, check } = require('express-validator');
const bcypt = require('bcrypt');
const salRounds = 8;
let jwt = require('jsonwebtoken');

class CuentaController{
async sesion(req, res){
    let errors = validationResult(req);
    if (errors.isEmpty()) {
      var login =  await cuenta.findOne({where: {usuario: req.body.correo}, include: [{
        model: models.persona, as: 'persona',
        attributes: ['nombres', 'apellidos']
    }]});
      if (login === null) {
        res.status(400);
        res.json({
            msg: "CUENTA NO ENCONTRADA",
            code: 400
        });
    } else {
        res.status(200);
        var isClavaValida = function(clave, claveUs){
            return bcypt.compareSync(claveUs, clave);
        };

        if (login.estado) {
            if (isClavaValida(login.clave, req.body.clave)) {
                const tokenData = {
                    external: login.external_id,
                    usuario: login.usuario,
                    check: true
                }; 
                require('dotenv').config();
                const llave=process.env.KEY;
                const token = jwt.sign(tokenData,llave , {expiresIn: "1h"});
                res.json({msg: 'OK!', toke: token, user: login.persona.nombres+ ' '+ login.persona.apellidos, code: 200, correo: login.correo});
            } else {
                res.json({
                    msg: "CLAVE INCORRECTA",
                    code: 400
                });
            }
            
        }else{
            res.json({
                msg: "CUENTA DESACTIVADA",
                code: 400
            });

        }
    }

    } else {
        res.status(400);
        res.json({ msg: "Faltan datos", code: 400 });
    }


}

}
module.exports =CuentaController;
