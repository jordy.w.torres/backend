'use strict';
module.exports = (sequalize, DataTypes) => {
    const persona = sequalize.define('persona', {
        nombres: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        apellidos: {type: DataTypes.STRING(50), defaultValue: "NO_DATA"},
        identificacion: {
            type: DataTypes.STRING(20),
            unique: true,
            allowNull: false,
            defaultValue: "NO_DATA"
        },
        tipo: {
            type: DataTypes.ENUM("CEDULA","PASAPORTE","RUC"),
            defaultValue: "CEDULA"
        },
        direccion: {
            type: DataTypes.STRING,
            allowNull: true
        },
        fecha_Nacimiento: {type: DataTypes.STRING(50),defaultValue:"No_data"},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}
    }, {freezeTableName: true});
    persona.associate = function(models) {
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
        persona.hasMany(models.auto, {foreignKey: 'id_persona', as: 'auto'});
    }

    return persona;
}