var express = require('express');
var router = express.Router();
const { body, validationResult } = require('express-validator');
const RolController = require('../controller/RolController');
var rolController = new RolController();
const CuentaController = require('../controller/CuentaController');
var cuentaController = new CuentaController();
const PersonaController = require('../controller/PersonaController');
var personaController = new PersonaController();
const AutoController = require('../controller/AutoController');
var autoController = new AutoController();
const MarcaController = require('../controller/MarcaController');
var marcaController = new MarcaController();
const { decode } = require('jsonwebtoken');


var auth = function middleware(req, res, next) {
  console.log("hola");
  const token = req.headers['x-api-token'];
  console.log(req.headers);
  console.log(token);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log(err)
        res.status(401);
        res.send("token no valido");
        res.json({ msg: "Token no valido", code: 401 });
      } else {
        var models = require("../models")
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (aux === null) {
          res.status(401);
          res.send("token no valido");
          res.json({ msg: "Token no valido", code: 401 });
        } else {
          next();
        }

      }
    })
  } else {
    res.status(401);
    res.send("no existe el token");
    res.json({ msg: "No existe el token!!", code: 401 })
  }

}
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
router.get('/roles', rolController.listar);
//
router.put('/personas/modificar', personaController.modificar);

router.put('/autos/modificar', autoController.modificar);

router.post('/sesion', cuentaController.sesion);
router.post('/personas/guardar', personaController.guardar, [
  body('apellidos', 'Ingrese su apellido').exists(),
  body('nombres', 'Ingrese algun dato').exists()
]);
//router.put('/autos/modificar', autoController.);

router.post('/autos/guardar', autoController.guardar);

router.get('/autos/listar', autoController.listar);
router.get('/autos/cantidad', autoController.cantAutos);
router.get('/marcas/listar', marcaController.listar);
router.get('/marcas/cantidad', marcaController.cantMarcas);
router.get('/personas', auth, personaController.listar);
router.get('/personas/listar', personaController.listar);
router.get('/personas/obtener/:external', auth, personaController.obtener);
module.exports = router;
/*
router.get('/sumar/:a/:b', function(req, res, next) {
  var a= req.params.a*1;
  var b= Number(req.params.b);
  var c=a+b;
  res.status(200);
    res.json({"msg":"OK","resp":c});
  
 
});
router.post('/sumar', function(req, res, next) {
  var a= Number(req.body.a);
  var b= Number(req.body.b);
  if(isNaN(a)||isNaN(b)){
    res.status(400);
    res.json({"msg":"Error: Faltan datos..."});
  }
  var c=a+b;
  res.status(200);
    res.json({"msg":"OK","resp":c});
  
  });
  */